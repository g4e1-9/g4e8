//GUIA MANEJO STRINGS EJERCICIO 8
//El usuario ingresará nombres de personas hasta que ingrese la palabra "FIN".
//No sabemos cuántos nombres ingresará.
//Luego de finalizar el ingreso, mostrar en pantalla cuál es el primer nombre en orden alfabético de todos los ingresados.

#include <stdio.h>
#include <string.h>

int main()
{
    char primero[20]; //Declaro las variables.
    char final[5] = "FIN\n";
    char ingresado[20];

    printf("Ingrese un nombre: ");
    fgets(primero, 20, stdin);
    
    do {
        printf("Ingrese un nombre: ");
        fgets(ingresado, 20, stdin);
           
        if(strcmp(ingresado, final) == 0) //Detecto si se escribio la palabra "FIN".
        
        break;
        
        if (strcmp (primero, ingresado) > 0 ) //Comparo los nombres y detecto el que se ubique primero en orden alfabético. 
            {
            strcpy (primero, ingresado);
            }
        } 

while (1);

printf("\nEl primer nombre en orden alfabetico es: %s", primero); //Muestro en pantalla el primer nombre en orden alfabético.

return 0;

}